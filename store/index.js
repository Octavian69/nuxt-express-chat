export const state = () => ({
    user: {},
    users: [],
    messages: []
});



export const mutations = {
    setUser(state, user) {
        state.user = user;
    },
    reset(state) {
        state.user = {},
        state.messages = []
    },
    SOCKET_newMessage(state, data) {
        state.messages.push(data);
    },
    SOCKET_updateUsers(state, data) {
        console.log(data)
        state.users = data;
    }
}


