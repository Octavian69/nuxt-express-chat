const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const users = require('../db/Users')();

const m = (name, text, id) => ({name, text, id});

io.on('connection', (socket) => {

    socket.on('joinUser', (user, cb) => {
        const isInvalid = !user.room || !user.name;

        if(isInvalid) {
            return socket.emit({ status: 400, state: { message: "Некорректные данные" } });
        }

        cb({ status: 200, state: { id: socket.id } });

        users.remove(socket.id);
        users.add({ id: socket.id, ...user });

        socket.join(user.room);
        io.to(user.room).emit('updateUsers', users.getByRoom(user.room));
        socket.emit('newMessage', m('admin', `Добро пожаловать в беседу ${user.name}`));
        socket.broadcast.to(user.room).emit('newMessage', m('admin', `Пользователь ${user.name} присоединился к беседе`));
    });

    socket.on('createMessage', (message, cb) => {

        if(!message.text) {
            return cb({ status: 400, state: { message: "Некорректный ввод" } })
        }

        const { id } = message;
        const user = users.get(id);

        io.to(user.room).emit('newMessage', m(user.name, message.text, user.id));

        cb({status: 200, state: { message: 'Сообщение отправлено' }});
    });

    socket.on('userLeft', (data, cb) => {

        const user = users.remove(data.id); 

        if(user) {
            io.to(user.room).emit('updateUsers', users.getByRoom(user.room));
            io.to(user.room).emit('newMessage', m('admin', `Пользователь ${user.name} покинул беседу.`, user.id));
            socket.emit('newMessage ', users.getByRoom(user.room));
            
        } 

        cb({status: 200, state: { message: 'Успешно покинул чат' } });
    });

    socket.on('disconnect', () => {
        const user = users.remove(socket.id);

        if(user) {
            io.to(user.room).emit('updateUsers', users.getByRoom(user.room));
            io.to(user.room).emit('newMessage', m('admin', `Пользователь ${user.name} покинул беседу.`, user.id));
        }
    });
})

module.exports = { app, server };